"""
Get stat for tournament data
"""
# pylint: disable=no-member
# pylint: disable=unsubscriptable-object
# pylint: disable=unsupported-assignment-operation

import click
import pandas as pd

# INPUT_PATH = "../../data/raw/WNCAATourneyDetailedResults.csv"
# OUTPUT_PATH = "../../data/interim/w_ncaa.csv"


@click.command()
@click.argument("input_path", type=click.Path())
@click.argument("output_path", type=click.Path())

def collect_ncaa_stat(input_path: str, output_path: str):
    """
    Function for raw data preprocessing
    :param input_path: Path to read data
    :param output_path: Path to save data
    :return:
    """


    # Чтение файла, можем подставить мужские/женские команды
    df_main = pd.read_csv(input_path)
    df_main.drop("Unnamed: 0", axis=1, inplace=True)

    # За всё время каждая команда во всех турнирах

    # кол-во побед во всех турнирах
    ncaa_total_w_count = (
        df_main.groupby(["TeamID"])["ncaa_w_count"]
        .sum()
        .reset_index()
        .drop_duplicates()
        .rename(columns={"ncaa_w_count": "ncaa_total_w_count"})
    )

    # кол-во поражений во всех турнирах
    ncaa_total_l_count = (
        df_main.groupby(["TeamID"])["ncaa_l_count"]
        .sum()
        .reset_index()
        .drop_duplicates()
        .rename(columns={"ncaa_l_count": "ncaa_total_l_count"})
    )
    df_fseason = ncaa_total_w_count.merge(ncaa_total_l_count, on=["TeamID"], how="left")

    # процент побед во всех турнирах
    df_fseason["ncaa_total_winrate"] = df_fseason["ncaa_total_w_count"] / (
        df_fseason["ncaa_total_l_count"] + df_fseason["ncaa_total_w_count"]
    )

    # средняя разница забитых/пропущенных в выйгрышных матчах во всех турнирах
    ncaa_total_avg_w_score_diff = (
        df_main.groupby(["TeamID"])["ncaa_w_avg_score_diff"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_w_avg_score_diff": "ncaa_total_avg_w_score_diff"})
    )
    df_fseason = df_fseason.merge(ncaa_total_avg_w_score_diff, on=["TeamID"], how="left")

    # средняя разница забитых/пропущенных в пройгрышных матчах во всех турнирах
    ncaa_total_avg_l_score_diff = (
        df_main.groupby(["TeamID"])["ncaa_l_avg_score_diff"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_l_avg_score_diff": "ncaa_total_avg_l_score_diff"})
    )
    df_fseason = df_fseason.merge(ncaa_total_avg_l_score_diff, on=["TeamID"], how="left")

    # среднее кол-во забитых очков в каждой игре во всех турнирах
    ncaa_total_avg_self_pts = (
        df_main.groupby(["TeamID"])["ncaa_self_pts_per_game"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_self_pts_per_game": "ncaa_total_avg_self_pts"})
    )
    df_fseason = df_fseason.merge(ncaa_total_avg_self_pts, on=["TeamID"], how="left")

    # среднее кол-во пропущенных очков в каждой игре во всех турнирах
    ncaa_total_avg_opp_pts = (
        df_main.groupby(["TeamID"])["ncaa_opp_pts_per_game"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_opp_pts_per_game": "ncaa_total_avg_opp_pts"})
    )
    df_fseason = df_fseason.merge(ncaa_total_avg_opp_pts, on=["TeamID"], how="left")

    # процент забитых бросков с игры во всех турнирах
    ncaa_total_rate_fg = (
        df_main.groupby(["TeamID"])["ncaa_%fg"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg": "ncaa_total_rate_fg"})
    )
    df_fseason = df_fseason.merge(ncaa_total_rate_fg, on=["TeamID"], how="left")

    # процент забитых средних бросков во всех турнирах
    ncaa_total_rate_fg2 = (
        df_main.groupby(["TeamID"])["ncaa_%fg2"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg2": "ncaa_total_rate_fg2"})
    )
    df_fseason = df_fseason.merge(ncaa_total_rate_fg2, on=["TeamID"], how="left")

    # процент забитых 3ех бросков во всех турнирах
    ncaa_total_rate_fg3 = (
        df_main.groupby(["TeamID"])["ncaa_%fg3"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg3": "ncaa_total_rate_fg3"})
    )
    df_fseason = df_fseason.merge(ncaa_total_rate_fg3, on=["TeamID"], how="left")

    # процент забитых штрафных бросков во всех турнирах
    ncaa_total_rate_ft = (
        df_main.groupby(["TeamID"])["ncaa_%ft"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%ft": "ncaa_total_rate_ft"})
    )
    df_fseason = df_fseason.merge(ncaa_total_rate_ft, on=["TeamID"], how="left")

    # среднее кол-во подборов в атаке во всех турнирах
    ncaa_total_avg_or = (
        df_main.groupby(["TeamID"])["ncaa_avg_or"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_or": "ncaa_total_avg_or"})
    )
    df_fseason = df_fseason.merge(ncaa_total_avg_or, on=["TeamID"], how="left")

    # среднее кол-во подборов в защите во всех турнирах
    ncaa_total_avg_dr = (
        df_main.groupby(["TeamID"])["ncaa_avg_dr"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_dr": "ncaa_total_avg_dr"})
    )
    df_fseason = df_fseason.merge(ncaa_total_avg_dr, on=["TeamID"], how="left")

    # среднее кол-во голевых передач во всех турнирах
    ncaa_total_avg_ast = (
        df_main.groupby(["TeamID"])["ncaa_avg_ast"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_ast": "ncaa_total_avg_ast"})
    )
    df_fseason = df_fseason.merge(ncaa_total_avg_ast, on=["TeamID"], how="left")

    # среднее кол-во потерь во всех турнирах
    ncaa_total_avg_to = (
        df_main.groupby(["TeamID"])["ncaa_avg_to"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_to": "ncaa_total_avg_to"})
    )
    df_fseason = df_fseason.merge(ncaa_total_avg_to, on=["TeamID"], how="left")

    # среднее кол-во перехватов во всех турнирах
    ncaa_total_avg_stl = (
        df_main.groupby(["TeamID"])["ncaa_avg_stl"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_stl": "ncaa_total_avg_stl"})
    )
    df_fseason = df_fseason.merge(ncaa_total_avg_stl, on=["TeamID"], how="left")

    # среднее кол-во блоков во всех турнирах
    ncaa_total_avg_blk = (
        df_main.groupby(["TeamID"])["ncaa_avg_blk"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_blk": "ncaa_total_avg_blk"})
    )
    df_fseason = df_fseason.merge(ncaa_total_avg_blk, on=["TeamID"], how="left")

    # среднее кол-во персональных замечаний во всех турнирах
    ncaa_total_avg_pf = (
        df_main.groupby(["TeamID"])["ncaa_avg_pf"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_pf": "ncaa_total_avg_pf"})
    )
    df_fseason = df_fseason.merge(ncaa_total_avg_pf, on=["TeamID"], how="left")

    # За последний турнир команды, которые принимали участие

    df1 = df_main.loc[(df_main["Season"] == 2022)]

    ncaa_last_season_w_count = (
        df1.groupby(["TeamID"])["ncaa_w_count"]
        .sum()
        .reset_index()
        .drop_duplicates()
        .rename(columns={"ncaa_w_count": "ncaa_last_season_w_count"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_w_count, on=["TeamID"], how="left")

    ncaa_last_season_l_count = (
        df1.groupby(["TeamID"])["ncaa_l_count"]
        .sum()
        .reset_index()
        .drop_duplicates()
        .rename(columns={"ncaa_l_count": "ncaa_last_season_l_count"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_l_count, on=["TeamID"], how="left")

    df_fseason["ncaa_last_season_winrate"] = df_fseason["ncaa_last_season_w_count"] / (
        df_fseason["ncaa_last_season_l_count"] + df_fseason["ncaa_last_season_w_count"]
    )

    ncaa_last_season_avg_w_score_diff = (
        df1.groupby(["TeamID"])["ncaa_w_avg_score_diff"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_w_avg_score_diff": "ncaa_last_season_avg_w_score_diff"})
    )
    df_fseason = df_fseason.merge(
        ncaa_last_season_avg_w_score_diff, on=["TeamID"], how="left"
    )

    ncaa_last_season_avg_l_score_diff = (
        df1.groupby(["TeamID"])["ncaa_l_avg_score_diff"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_l_avg_score_diff": "ncaa_last_season_avg_l_score_diff"})
    )
    df_fseason = df_fseason.merge(
        ncaa_last_season_avg_l_score_diff, on=["TeamID"], how="left"
    )

    ncaa_last_season_avg_self_pts = (
        df1.groupby(["TeamID"])["ncaa_self_pts_per_game"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_self_pts_per_game": "ncaa_last_season_avg_self_pts"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_avg_self_pts, on=["TeamID"], how="left")

    ncaa_last_season_avg_opp_pts = (
        df1.groupby(["TeamID"])["ncaa_opp_pts_per_game"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_opp_pts_per_game": "ncaa_last_season_avg_opp_pts"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_avg_opp_pts, on=["TeamID"], how="left")

    ncaa_last_season_rate_fg = (
        df1.groupby(["TeamID"])["ncaa_%fg"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg": "ncaa_last_season_rate_fg"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_rate_fg, on=["TeamID"], how="left")

    ncaa_last_season_rate_fg2 = (
        df1.groupby(["TeamID"])["ncaa_%fg2"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg2": "ncaa_last_season_rate_fg2"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_rate_fg2, on=["TeamID"], how="left")

    ncaa_last_season_rate_fg3 = (
        df1.groupby(["TeamID"])["ncaa_%fg3"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg3": "ncaa_last_season_rate_fg3"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_rate_fg3, on=["TeamID"], how="left")

    ncaa_last_season_rate_ft = (
        df1.groupby(["TeamID"])["ncaa_%ft"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%ft": "ncaa_last_season_rate_ft"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_rate_ft, on=["TeamID"], how="left")

    ncaa_last_season_avg_or = (
        df1.groupby(["TeamID"])["ncaa_avg_or"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_or": "ncaa_last_season_avg_or"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_avg_or, on=["TeamID"], how="left")

    ncaa_last_season_avg_dr = (
        df1.groupby(["TeamID"])["ncaa_avg_dr"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_dr": "ncaa_last_season_avg_dr"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_avg_dr, on=["TeamID"], how="left")

    ncaa_last_season_avg_ast = (
        df1.groupby(["TeamID"])["ncaa_avg_ast"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_ast": "ncaa_last_season_avg_ast"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_avg_ast, on=["TeamID"], how="left")

    ncaa_last_season_avg_to = (
        df1.groupby(["TeamID"])["ncaa_avg_to"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_to": "ncaa_last_season_avg_to"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_avg_to, on=["TeamID"], how="left")

    ncaa_last_season_avg_stl = (
        df1.groupby(["TeamID"])["ncaa_avg_stl"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_stl": "ncaa_last_season_avg_stl"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_avg_stl, on=["TeamID"], how="left")

    ncaa_last_season_avg_blk = (
        df1.groupby(["TeamID"])["ncaa_avg_blk"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_blk": "ncaa_last_season_avg_blk"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_avg_blk, on=["TeamID"], how="left")

    ncaa_last_season_avg_pf = (
        df1.groupby(["TeamID"])["ncaa_avg_pf"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_pf": "ncaa_last_season_avg_pf"})
    )
    df_fseason = df_fseason.merge(ncaa_last_season_avg_pf, on=["TeamID"], how="left")

    # За последние 3 турнира команды, которые принимали участие

    df3 = df_main.loc[(df_main["Season"] <= 2022) & (df_main["Season"] > 2019)]

    ncaa_last3seasons_w_count = (
        df3.groupby(["TeamID"])["ncaa_w_count"]
        .sum()
        .reset_index()
        .drop_duplicates()
        .rename(columns={"ncaa_w_count": "ncaa_last3seasons_w_count"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_w_count, on=["TeamID"], how="left")

    ncaa_last3seasons_l_count = (
        df3.groupby(["TeamID"])["ncaa_l_count"]
        .sum()
        .reset_index()
        .drop_duplicates()
        .rename(columns={"ncaa_l_count": "ncaa_last3seasons_l_count"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_l_count, on=["TeamID"], how="left")

    df_fseason["ncaa_last3seasons_winrate"] = df_fseason["ncaa_last3seasons_w_count"] / (
        df_fseason["ncaa_last3seasons_l_count"] + df_fseason["ncaa_last3seasons_w_count"]
    )

    ncaa_last3seasons_avg_w_score_diff = (
        df3.groupby(["TeamID"])["ncaa_w_avg_score_diff"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_w_avg_score_diff": "ncaa_last3seasons_avg_w_score_diff"})
    )
    df_fseason = df_fseason.merge(
        ncaa_last3seasons_avg_w_score_diff, on=["TeamID"], how="left"
    )

    ncaa_last3seasons_avg_l_score_diff = (
        df3.groupby(["TeamID"])["ncaa_l_avg_score_diff"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_l_avg_score_diff": "ncaa_last3seasons_avg_l_score_diff"})
    )
    df_fseason = df_fseason.merge(
        ncaa_last3seasons_avg_l_score_diff, on=["TeamID"], how="left"
    )

    ncaa_last3seasons_avg_self_pts = (
        df3.groupby(["TeamID"])["ncaa_self_pts_per_game"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_self_pts_per_game": "ncaa_last3seasons_avg_self_pts"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_avg_self_pts, on=["TeamID"], how="left")

    ncaa_last3seasons_avg_opp_pts = (
        df3.groupby(["TeamID"])["ncaa_opp_pts_per_game"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_opp_pts_per_game": "ncaa_last3seasons_avg_opp_pts"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_avg_opp_pts, on=["TeamID"], how="left")

    ncaa_last3seasons_rate_fg = (
        df3.groupby(["TeamID"])["ncaa_%fg"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg": "ncaa_last3seasons_rate_fg"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_rate_fg, on=["TeamID"], how="left")

    ncaa_last3seasons_rate_fg2 = (
        df3.groupby(["TeamID"])["ncaa_%fg2"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg2": "ncaa_last3seasons_rate_fg2"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_rate_fg2, on=["TeamID"], how="left")

    ncaa_last3seasons_rate_fg3 = (
        df3.groupby(["TeamID"])["ncaa_%fg3"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg3": "ncaa_last3seasons_rate_fg3"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_rate_fg3, on=["TeamID"], how="left")

    ncaa_last3seasons_rate_ft = (
        df3.groupby(["TeamID"])["ncaa_%ft"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%ft": "ncaa_last3seasons_rate_ft"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_rate_ft, on=["TeamID"], how="left")

    ncaa_last3seasons_avg_or = (
        df3.groupby(["TeamID"])["ncaa_avg_or"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_or": "ncaa_last3seasons_avg_or"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_avg_or, on=["TeamID"], how="left")

    ncaa_last3seasons_avg_dr = (
        df3.groupby(["TeamID"])["ncaa_avg_dr"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_dr": "ncaa_last3seasons_avg_dr"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_avg_dr, on=["TeamID"], how="left")

    ncaa_last3seasons_avg_ast = (
        df3.groupby(["TeamID"])["ncaa_avg_ast"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_ast": "ncaa_last3seasons_avg_ast"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_avg_ast, on=["TeamID"], how="left")

    ncaa_last3seasons_avg_to = (
        df3.groupby(["TeamID"])["ncaa_avg_to"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_to": "ncaa_last3seasons_avg_to"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_avg_to, on=["TeamID"], how="left")

    ncaa_last3seasons_avg_stl = (
        df3.groupby(["TeamID"])["ncaa_avg_stl"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_stl": "ncaa_last3seasons_avg_stl"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_avg_stl, on=["TeamID"], how="left")

    ncaa_last3seasons_avg_blk = (
        df3.groupby(["TeamID"])["ncaa_avg_blk"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_blk": "ncaa_last3seasons_avg_blk"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_avg_blk, on=["TeamID"], how="left")

    ncaa_last3seasons_avg_pf = (
        df3.groupby(["TeamID"])["ncaa_avg_pf"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_pf": "ncaa_last3seasons_avg_pf"})
    )
    df_fseason = df_fseason.merge(ncaa_last3seasons_avg_pf, on=["TeamID"], how="left")

    # За последние 5 турниров команды, которые принимали участие

    df5 = df_main.loc[(df_main["Season"] < 2023) & (df_main["Season"] > 2017)]

    ncaa_last5seasons_w_count = (
        df5.groupby(["TeamID"])["ncaa_w_count"]
        .sum()
        .reset_index()
        .drop_duplicates()
        .rename(columns={"ncaa_w_count": "ncaa_last5seasons_w_count"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_w_count, on=["TeamID"], how="left")

    ncaa_last5seasons_l_count = (
        df5.groupby(["TeamID"])["ncaa_l_count"]
        .sum()
        .reset_index()
        .drop_duplicates()
        .rename(columns={"ncaa_l_count": "ncaa_last5seasons_l_count"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_l_count, on=["TeamID"], how="left")

    df_fseason["ncaa_last5seasons_winrate"] = df_fseason["ncaa_last5seasons_w_count"] / (
        df_fseason["ncaa_last5seasons_l_count"] + df_fseason["ncaa_last5seasons_w_count"]
    )

    ncaa_last5seasons_avg_w_score_diff = (
        df5.groupby(["TeamID"])["ncaa_w_avg_score_diff"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_w_avg_score_diff": "ncaa_last5seasons_avg_w_score_diff"})
    )
    df_fseason = df_fseason.merge(
        ncaa_last5seasons_avg_w_score_diff, on=["TeamID"], how="left"
    )

    ncaa_last5seasons_avg_l_score_diff = (
        df5.groupby(["TeamID"])["ncaa_l_avg_score_diff"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_l_avg_score_diff": "ncaa_last5seasons_avg_l_score_diff"})
    )
    df_fseason = df_fseason.merge(
        ncaa_last5seasons_avg_l_score_diff, on=["TeamID"], how="left"
    )

    ncaa_last5seasons_avg_self_pts = (
        df5.groupby(["TeamID"])["ncaa_self_pts_per_game"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_self_pts_per_game": "ncaa_last5seasons_avg_self_pts"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_avg_self_pts, on=["TeamID"], how="left")

    ncaa_last5seasons_avg_opp_pts = (
        df5.groupby(["TeamID"])["ncaa_opp_pts_per_game"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_opp_pts_per_game": "ncaa_last5seasons_avg_opp_pts"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_avg_opp_pts, on=["TeamID"], how="left")

    ncaa_last5seasons_rate_fg = (
        df5.groupby(["TeamID"])["ncaa_%fg"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg": "ncaa_last5seasons_rate_fg"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_rate_fg, on=["TeamID"], how="left")

    ncaa_last5seasons_rate_fg2 = (
        df5.groupby(["TeamID"])["ncaa_%fg2"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg2": "ncaa_last5seasons_rate_fg2"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_rate_fg2, on=["TeamID"], how="left")

    ncaa_last5seasons_rate_fg3 = (
        df5.groupby(["TeamID"])["ncaa_%fg3"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%fg3": "ncaa_last5seasons_rate_fg3"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_rate_fg3, on=["TeamID"], how="left")

    ncaa_last5seasons_rate_ft = (
        df5.groupby(["TeamID"])["ncaa_%ft"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_%ft": "ncaa_last5seasons_rate_ft"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_rate_ft, on=["TeamID"], how="left")

    ncaa_last5seasons_avg_or = (
        df5.groupby(["TeamID"])["ncaa_avg_or"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_or": "ncaa_last5seasons_avg_or"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_avg_or, on=["TeamID"], how="left")

    ncaa_last5seasons_avg_dr = (
        df5.groupby(["TeamID"])["ncaa_avg_dr"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_dr": "ncaa_last5seasons_avg_dr"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_avg_dr, on=["TeamID"], how="left")

    ncaa_last5seasons_avg_ast = (
        df5.groupby(["TeamID"])["ncaa_avg_ast"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_ast": "ncaa_last5seasons_avg_ast"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_avg_ast, on=["TeamID"], how="left")

    ncaa_last5seasons_avg_to = (
        df5.groupby(["TeamID"])["ncaa_avg_to"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_to": "ncaa_last5seasons_avg_to"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_avg_to, on=["TeamID"], how="left")

    ncaa_last5seasons_avg_stl = (
        df5.groupby(["TeamID"])["ncaa_avg_stl"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_stl": "ncaa_last5seasons_avg_stl"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_avg_stl, on=["TeamID"], how="left")

    ncaa_last5seasons_avg_blk = (
        df5.groupby(["TeamID"])["ncaa_avg_blk"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_blk": "ncaa_last5seasons_avg_blk"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_avg_blk, on=["TeamID"], how="left")

    ncaa_last5seasons_avg_pf = (
        df5.groupby(["TeamID"])["ncaa_avg_pf"]
        .mean()
        .reset_index()
        .rename(columns={"ncaa_avg_pf": "ncaa_last5seasons_avg_pf"})
    )
    df_fseason = df_fseason.merge(ncaa_last5seasons_avg_pf, on=["TeamID"], how="left")

    # Заполняем нулями те команды, которые не участвовали
    # во всех турнирах за последние 5 сезонов

    df_fseason.fillna(0, inplace=True)

    df_fseason.to_csv(output_path)
    print("Your results saved")

    return None


collect_ncaa_stat()
