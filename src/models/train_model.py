"""This script is used to train a baseline model for a given dataset
and give a quick validation metrics"""

import warnings
import pickle
import pandas as pd
from sklearn.metrics import classification_report, accuracy_score, roc_auc_score
from sklearn.model_selection import train_test_split
from lightgbm import Dataset, train

warnings.filterwarnings("ignore")
pd.options.display.max_columns = 99
pd.options.display.max_rows = 150
pd.options.display.float_format = "{:.2f}".format


df = pd.read_csv("")

X = df.drop(columns=["Survived", "PassengerId"])
y = df.Survived  # таргет

for col in X.select_dtypes("object").columns:
    X[col] = X[col].astype("category")  # определим категориальные фичи

print(y.mean())

# train-test - holdout split
X_train, X_holdout, y_train, y_holdout = train_test_split(
    X, y, test_size=0.5, stratify=y, random_state=17
)
X_train, X_test, y_train, y_test = train_test_split(
    X_train, y_train, test_size=0.3, stratify=y_train, random_state=17
)

lgbm_params = {
    "objective": "binary",
    "verbosity": -1,
    "seed": 17,
    "learning_rate": 0.05,
}


# построим модель:
train_data = Dataset(X_train, y_train)
test_data = Dataset(X_test, y_test)

# train the model
clf = train(
    lgbm_params,
    train_data,
    valid_sets=test_data,
    early_stopping_rounds=1050,
    verbose_eval=500,
)  # train the model on 100 epocs
# prediction on the test set

lgbm_params_retrain = {
    "objective": "binary",
    "verbosity": -1,
    "seed": 42,
    "learning_rate": 0.01,
    "n_estimators": 200,
}

# train the model
clf_retrained = train(
    lgbm_params_retrain,
    Dataset(X, y),
    verbose_eval=500,
)  # train the model on 100 epocs
# prediction on the test set

clf_retrained.save_model("../../models/model_baseline.pkl")

with open("../../data/processed/features_model.pickle", "wb") as f:
    pickle.dump(X.columns.to_list(), f)

with open("../../data/processed/lgbm_params.pickle", "wb") as f:
    pickle.dump(lgbm_params, f)

roc_auc = round(roc_auc_score(y_holdout, clf.predict(X_holdout)), 3)
print(roc_auc)

y_pred = clf.predict(X_holdout)

print(accuracy_score(y_holdout, (y_pred > 0.5).astype("int")))

print(classification_report(y_holdout, (y_pred > 0.5).astype("int")))

print(pd.Series(y_holdout).value_counts(1))


# Feature importance
feature_importance = pd.DataFrame()
feature_importance["feature"] = X.columns
feature_importance["importance"] = clf.feature_importance()

# Комментарий - здесь должен появится график!
cols = (
    feature_importance[["feature", "importance"]]
    .groupby("feature")
    .mean()
    .sort_values(by="importance", ascending=False)[:30]
    .index
)
best_features = feature_importance.loc[feature_importance.feature.isin(cols)]
dd = best_features.sort_values(by="importance", ascending=False)
