"""
Collect stat for  regular season
"""
# pylint: disable=no-member
# pylint: disable=unsubscriptable-object
# pylint: disable=unsupported-assignment-operation

import pandas as pd

# Чтение файла, можем подставить мужские/женские команды
df_main = pd.read_csv("../data/interim/m_reg.csv")
df_main.drop("Unnamed: 0", axis=1, inplace=True)

# За всё время каждая команда во всех сезонах

# кол-во побед во всех сезонах
reg_total_w_count = (
    df_main.groupby(["TeamID"])["reg_w_count"]
    .sum()
    .reset_index()
    .drop_duplicates()
    .rename(columns={"reg_w_count": "reg_total_w_count"})
)

# кол-во поражений во всех сезонах
reg_total_l_count = (
    df_main.groupby(["TeamID"])["reg_l_count"]
    .sum()
    .reset_index()
    .drop_duplicates()
    .rename(columns={"reg_l_count": "reg_total_l_count"})
)
df_fseason = reg_total_w_count.merge(reg_total_l_count, on=["TeamID"], how="left")

# процент побед во всех сезонах
df_fseason["reg_total_winrate"] = df_fseason["reg_total_w_count"] / (
    df_fseason["reg_total_l_count"] + df_fseason["reg_total_w_count"]
)

# средняя разница забитых/пропущенных в выйгрышных матчах во всех сезонах
reg_total_avg_w_score_diff = (
    df_main.groupby(["TeamID"])["reg_w_avg_score_diff"]
    .mean()
    .reset_index()
    .rename(columns={"reg_w_avg_score_diff": "reg_total_avg_w_score_diff"})
)
df_fseason = df_fseason.merge(reg_total_avg_w_score_diff, on=["TeamID"], how="left")

# средняя разница забитых/пропущенных в пройгрышных матчах во всех сезонах
reg_total_avg_l_score_diff = (
    df_main.groupby(["TeamID"])["reg_l_avg_score_diff"]
    .mean()
    .reset_index()
    .rename(columns={"reg_l_avg_score_diff": "reg_total_avg_l_score_diff"})
)
df_fseason = df_fseason.merge(reg_total_avg_l_score_diff, on=["TeamID"], how="left")

# среднее кол-во забитых очков в каждой игре во всех сезонах
reg_total_avg_self_pts = (
    df_main.groupby(["TeamID"])["reg_self_pts_per_game"]
    .mean()
    .reset_index()
    .rename(columns={"reg_self_pts_per_game": "reg_total_avg_self_pts"})
)
df_fseason = df_fseason.merge(reg_total_avg_self_pts, on=["TeamID"], how="left")

# среднее кол-во пропущенных очков в каждой игре во всех сезонах
reg_total_avg_opp_pts = (
    df_main.groupby(["TeamID"])["reg_opp_pts_per_game"]
    .mean()
    .reset_index()
    .rename(columns={"reg_opp_pts_per_game": "reg_total_avg_opp_pts"})
)
df_fseason = df_fseason.merge(reg_total_avg_opp_pts, on=["TeamID"], how="left")

# процент забитых бросков с игры во всех сезонах
reg_total_rate_fg = (
    df_main.groupby(["TeamID"])["reg_%fg"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg": "reg_total_rate_fg"})
)
df_fseason = df_fseason.merge(reg_total_rate_fg, on=["TeamID"], how="left")

# процент забитых средних бросков во всех сезонах
reg_total_rate_fg2 = (
    df_main.groupby(["TeamID"])["reg_%fg2"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg2": "reg_total_rate_fg2"})
)
df_fseason = df_fseason.merge(reg_total_rate_fg2, on=["TeamID"], how="left")

# процент забитых 3ех бросков во всех сезонах
reg_total_rate_fg3 = (
    df_main.groupby(["TeamID"])["reg_%fg3"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg3": "reg_total_rate_fg3"})
)
df_fseason = df_fseason.merge(reg_total_rate_fg3, on=["TeamID"], how="left")

# процент забитых штрафных бросков во всех сезонах
reg_total_rate_ft = (
    df_main.groupby(["TeamID"])["reg_%ft"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%ft": "reg_total_rate_ft"})
)
df_fseason = df_fseason.merge(reg_total_rate_ft, on=["TeamID"], how="left")

# среднее кол-во подборов в атаке во всех сезонах
reg_total_avg_or = (
    df_main.groupby(["TeamID"])["reg_avg_or"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_or": "reg_total_avg_or"})
)
df_fseason = df_fseason.merge(reg_total_avg_or, on=["TeamID"], how="left")

# среднее кол-во подборов в защите во всех сезонах
reg_total_avg_dr = (
    df_main.groupby(["TeamID"])["reg_avg_dr"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_dr": "reg_total_avg_dr"})
)
df_fseason = df_fseason.merge(reg_total_avg_dr, on=["TeamID"], how="left")

# среднее кол-во голевых передач во всех сезонах
reg_total_avg_ast = (
    df_main.groupby(["TeamID"])["reg_avg_ast"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_ast": "reg_total_avg_ast"})
)
df_fseason = df_fseason.merge(reg_total_avg_ast, on=["TeamID"], how="left")

# среднее кол-во потерь во всех сезонах
reg_total_avg_to = (
    df_main.groupby(["TeamID"])["reg_avg_to"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_to": "reg_total_avg_to"})
)
df_fseason = df_fseason.merge(reg_total_avg_to, on=["TeamID"], how="left")

# среднее кол-во перехватов во всех сезонах
reg_total_avg_stl = (
    df_main.groupby(["TeamID"])["reg_avg_stl"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_stl": "reg_total_avg_stl"})
)
df_fseason = df_fseason.merge(reg_total_avg_stl, on=["TeamID"], how="left")

# среднее кол-во блоков во всех сезонах
reg_total_avg_blk = (
    df_main.groupby(["TeamID"])["reg_avg_blk"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_blk": "reg_total_avg_blk"})
)
df_fseason = df_fseason.merge(reg_total_avg_blk, on=["TeamID"], how="left")

# среднее кол-во персональных замечаний во всех сезонах
reg_total_avg_pf = (
    df_main.groupby(["TeamID"])["reg_avg_pf"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_pf": "reg_total_avg_pf"})
)
df_fseason = df_fseason.merge(reg_total_avg_pf, on=["TeamID"], how="left")

# За последний сезон каждая команда

df1 = df_main.loc[(df_main["Season"] == 2023)]

reg_last_season_w_count = (
    df1.groupby(["TeamID"])["reg_w_count"]
    .sum()
    .reset_index()
    .drop_duplicates()
    .rename(columns={"reg_w_count": "reg_last_season_w_count"})
)
df_fseason = df_fseason.merge(reg_last_season_w_count, on=["TeamID"], how="left")

reg_last_season_l_count = (
    df1.groupby(["TeamID"])["reg_l_count"]
    .sum()
    .reset_index()
    .drop_duplicates()
    .rename(columns={"reg_l_count": "reg_last_season_l_count"})
)
df_fseason = df_fseason.merge(reg_last_season_l_count, on=["TeamID"], how="left")

df_fseason["reg_last_season_winrate"] = df_fseason["reg_last_season_w_count"] / (
    df_fseason["reg_last_season_l_count"] + df_fseason["reg_last_season_w_count"]
)

reg_last_season_avg_w_score_diff = (
    df1.groupby(["TeamID"])["reg_w_avg_score_diff"]
    .mean()
    .reset_index()
    .rename(columns={"reg_w_avg_score_diff": "reg_last_season_avg_w_score_diff"})
)
df_fseason = df_fseason.merge(
    reg_last_season_avg_w_score_diff, on=["TeamID"], how="left"
)

reg_last_season_avg_l_score_diff = (
    df1.groupby(["TeamID"])["reg_l_avg_score_diff"]
    .mean()
    .reset_index()
    .rename(columns={"reg_l_avg_score_diff": "reg_last_season_avg_l_score_diff"})
)
df_fseason = df_fseason.merge(
    reg_last_season_avg_l_score_diff, on=["TeamID"], how="left"
)

reg_last_season_avg_self_pts = (
    df1.groupby(["TeamID"])["reg_self_pts_per_game"]
    .mean()
    .reset_index()
    .rename(columns={"reg_self_pts_per_game": "reg_last_season_avg_self_pts"})
)
df_fseason = df_fseason.merge(reg_last_season_avg_self_pts, on=["TeamID"], how="left")

reg_last_season_avg_opp_pts = (
    df1.groupby(["TeamID"])["reg_opp_pts_per_game"]
    .mean()
    .reset_index()
    .rename(columns={"reg_opp_pts_per_game": "reg_last_season_avg_opp_pts"})
)
df_fseason = df_fseason.merge(reg_last_season_avg_opp_pts, on=["TeamID"], how="left")

reg_last_season_rate_fg = (
    df1.groupby(["TeamID"])["reg_%fg"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg": "reg_last_season_rate_fg"})
)
df_fseason = df_fseason.merge(reg_last_season_rate_fg, on=["TeamID"], how="left")

reg_last_season_rate_fg2 = (
    df1.groupby(["TeamID"])["reg_%fg2"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg2": "reg_last_season_rate_fg2"})
)
df_fseason = df_fseason.merge(reg_last_season_rate_fg2, on=["TeamID"], how="left")

reg_last_season_rate_fg3 = (
    df1.groupby(["TeamID"])["reg_%fg3"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg3": "reg_last_season_rate_fg3"})
)
df_fseason = df_fseason.merge(reg_last_season_rate_fg3, on=["TeamID"], how="left")

reg_last_season_rate_ft = (
    df1.groupby(["TeamID"])["reg_%ft"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%ft": "reg_last_season_rate_ft"})
)
df_fseason = df_fseason.merge(reg_last_season_rate_ft, on=["TeamID"], how="left")

reg_last_season_avg_or = (
    df1.groupby(["TeamID"])["reg_avg_or"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_or": "reg_last_season_avg_or"})
)
df_fseason = df_fseason.merge(reg_last_season_avg_or, on=["TeamID"], how="left")

reg_last_season_avg_dr = (
    df1.groupby(["TeamID"])["reg_avg_dr"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_dr": "reg_last_season_avg_dr"})
)
df_fseason = df_fseason.merge(reg_last_season_avg_dr, on=["TeamID"], how="left")

reg_last_season_avg_ast = (
    df1.groupby(["TeamID"])["reg_avg_ast"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_ast": "reg_last_season_avg_ast"})
)
df_fseason = df_fseason.merge(reg_last_season_avg_ast, on=["TeamID"], how="left")

reg_last_season_avg_to = (
    df1.groupby(["TeamID"])["reg_avg_to"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_to": "reg_last_season_avg_to"})
)
df_fseason = df_fseason.merge(reg_last_season_avg_to, on=["TeamID"], how="left")

reg_last_season_avg_stl = (
    df1.groupby(["TeamID"])["reg_avg_stl"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_stl": "reg_last_season_avg_stl"})
)
df_fseason = df_fseason.merge(reg_last_season_avg_stl, on=["TeamID"], how="left")

reg_last_season_avg_blk = (
    df1.groupby(["TeamID"])["reg_avg_blk"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_blk": "reg_last_season_avg_blk"})
)
df_fseason = df_fseason.merge(reg_last_season_avg_blk, on=["TeamID"], how="left")

reg_last_season_avg_pf = (
    df1.groupby(["TeamID"])["reg_avg_pf"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_pf": "reg_last_season_avg_pf"})
)
df_fseason = df_fseason.merge(reg_last_season_avg_pf, on=["TeamID"], how="left")

# За последние 3 сезона каждая команда

df3 = df_main.loc[(df_main["Season"] <= 2023) & (df_main["Season"] > 2020)]

reg_last3seasons_w_count = (
    df3.groupby(["TeamID"])["reg_w_count"]
    .sum()
    .reset_index()
    .drop_duplicates()
    .rename(columns={"reg_w_count": "reg_last3seasons_w_count"})
)
df_fseason = df_fseason.merge(reg_last3seasons_w_count, on=["TeamID"], how="left")

reg_last3seasons_l_count = (
    df3.groupby(["TeamID"])["reg_l_count"]
    .sum()
    .reset_index()
    .drop_duplicates()
    .rename(columns={"reg_l_count": "reg_last3seasons_l_count"})
)
df_fseason = df_fseason.merge(reg_last3seasons_l_count, on=["TeamID"], how="left")

df_fseason["reg_last3seasons_winrate"] = df_fseason["reg_last3seasons_w_count"] / (
    df_fseason["reg_last3seasons_l_count"] + df_fseason["reg_last3seasons_w_count"]
)

reg_last3seasons_avg_w_score_diff = (
    df3.groupby(["TeamID"])["reg_w_avg_score_diff"]
    .mean()
    .reset_index()
    .rename(columns={"reg_w_avg_score_diff": "reg_last3seasons_avg_w_score_diff"})
)
df_fseason = df_fseason.merge(
    reg_last3seasons_avg_w_score_diff, on=["TeamID"], how="left"
)

reg_last3seasons_avg_l_score_diff = (
    df3.groupby(["TeamID"])["reg_l_avg_score_diff"]
    .mean()
    .reset_index()
    .rename(columns={"reg_l_avg_score_diff": "reg_last3seasons_avg_l_score_diff"})
)
df_fseason = df_fseason.merge(
    reg_last3seasons_avg_l_score_diff, on=["TeamID"], how="left"
)

reg_last3seasons_avg_self_pts = (
    df3.groupby(["TeamID"])["reg_self_pts_per_game"]
    .mean()
    .reset_index()
    .rename(columns={"reg_self_pts_per_game": "reg_last3seasons_avg_self_pts"})
)
df_fseason = df_fseason.merge(reg_last3seasons_avg_self_pts, on=["TeamID"], how="left")

reg_last3seasons_avg_opp_pts = (
    df3.groupby(["TeamID"])["reg_opp_pts_per_game"]
    .mean()
    .reset_index()
    .rename(columns={"reg_opp_pts_per_game": "reg_last3seasons_avg_opp_pts"})
)
df_fseason = df_fseason.merge(reg_last3seasons_avg_opp_pts, on=["TeamID"], how="left")

reg_last3seasons_rate_fg = (
    df3.groupby(["TeamID"])["reg_%fg"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg": "reg_last3seasons_rate_fg"})
)
df_fseason = df_fseason.merge(reg_last3seasons_rate_fg, on=["TeamID"], how="left")

reg_last3seasons_rate_fg2 = (
    df3.groupby(["TeamID"])["reg_%fg2"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg2": "reg_last3seasons_rate_fg2"})
)
df_fseason = df_fseason.merge(reg_last3seasons_rate_fg2, on=["TeamID"], how="left")

reg_last3seasons_rate_fg3 = (
    df3.groupby(["TeamID"])["reg_%fg3"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg3": "reg_last3seasons_rate_fg3"})
)
df_fseason = df_fseason.merge(reg_last3seasons_rate_fg3, on=["TeamID"], how="left")

reg_last3seasons_rate_ft = (
    df3.groupby(["TeamID"])["reg_%ft"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%ft": "reg_last3seasons_rate_ft"})
)
df_fseason = df_fseason.merge(reg_last3seasons_rate_ft, on=["TeamID"], how="left")

reg_last3seasons_avg_or = (
    df3.groupby(["TeamID"])["reg_avg_or"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_or": "reg_last3seasons_avg_or"})
)
df_fseason = df_fseason.merge(reg_last3seasons_avg_or, on=["TeamID"], how="left")

reg_last3seasons_avg_dr = (
    df3.groupby(["TeamID"])["reg_avg_dr"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_dr": "reg_last3seasons_avg_dr"})
)
df_fseason = df_fseason.merge(reg_last3seasons_avg_dr, on=["TeamID"], how="left")

reg_last3seasons_avg_ast = (
    df3.groupby(["TeamID"])["reg_avg_ast"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_ast": "reg_last3seasons_avg_ast"})
)
df_fseason = df_fseason.merge(reg_last3seasons_avg_ast, on=["TeamID"], how="left")

reg_last3seasons_avg_to = (
    df3.groupby(["TeamID"])["reg_avg_to"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_to": "reg_last3seasons_avg_to"})
)
df_fseason = df_fseason.merge(reg_last3seasons_avg_to, on=["TeamID"], how="left")

reg_last3seasons_avg_stl = (
    df3.groupby(["TeamID"])["reg_avg_stl"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_stl": "reg_last3seasons_avg_stl"})
)
df_fseason = df_fseason.merge(reg_last3seasons_avg_stl, on=["TeamID"], how="left")

reg_last3seasons_avg_blk = (
    df3.groupby(["TeamID"])["reg_avg_blk"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_blk": "reg_last3seasons_avg_blk"})
)
df_fseason = df_fseason.merge(reg_last3seasons_avg_blk, on=["TeamID"], how="left")

reg_last3seasons_avg_pf = (
    df3.groupby(["TeamID"])["reg_avg_pf"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_pf": "reg_last3seasons_avg_pf"})
)
df_fseason = df_fseason.merge(reg_last3seasons_avg_pf, on=["TeamID"], how="left")

# За последние 5 сезонов каждая команда

df5 = df_main.loc[(df_main["Season"] <= 2023) & (df_main["Season"] > 2018)]

reg_last5seasons_w_count = (
    df5.groupby(["TeamID"])["reg_w_count"]
    .sum()
    .reset_index()
    .drop_duplicates()
    .rename(columns={"reg_w_count": "reg_last5seasons_w_count"})
)
df_fseason = df_fseason.merge(reg_last5seasons_w_count, on=["TeamID"], how="left")

reg_last5seasons_l_count = (
    df5.groupby(["TeamID"])["reg_l_count"]
    .sum()
    .reset_index()
    .drop_duplicates()
    .rename(columns={"reg_l_count": "reg_last5seasons_l_count"})
)
df_fseason = df_fseason.merge(reg_last5seasons_l_count, on=["TeamID"], how="left")

df_fseason["reg_last5seasons_winrate"] = df_fseason["reg_last5seasons_w_count"] / (
    df_fseason["reg_last5seasons_l_count"] + df_fseason["reg_last5seasons_w_count"]
)

reg_last5seasons_avg_w_score_diff = (
    df5.groupby(["TeamID"])["reg_w_avg_score_diff"]
    .mean()
    .reset_index()
    .rename(columns={"reg_w_avg_score_diff": "reg_last5seasons_avg_w_score_diff"})
)
df_fseason = df_fseason.merge(
    reg_last5seasons_avg_w_score_diff, on=["TeamID"], how="left"
)

reg_last5seasons_avg_l_score_diff = (
    df5.groupby(["TeamID"])["reg_l_avg_score_diff"]
    .mean()
    .reset_index()
    .rename(columns={"reg_l_avg_score_diff": "reg_last5seasons_avg_l_score_diff"})
)
df_fseason = df_fseason.merge(
    reg_last5seasons_avg_l_score_diff, on=["TeamID"], how="left"
)

reg_last5seasons_avg_self_pts = (
    df5.groupby(["TeamID"])["reg_self_pts_per_game"]
    .mean()
    .reset_index()
    .rename(columns={"reg_self_pts_per_game": "reg_last5seasons_avg_self_pts"})
)
df_fseason = df_fseason.merge(reg_last5seasons_avg_self_pts, on=["TeamID"], how="left")

reg_last5seasons_avg_opp_pts = (
    df5.groupby(["TeamID"])["reg_opp_pts_per_game"]
    .mean()
    .reset_index()
    .rename(columns={"reg_opp_pts_per_game": "reg_last5seasons_avg_opp_pts"})
)
df_fseason = df_fseason.merge(reg_last5seasons_avg_opp_pts, on=["TeamID"], how="left")

reg_last5seasons_rate_fg = (
    df5.groupby(["TeamID"])["reg_%fg"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg": "reg_last5seasons_rate_fg"})
)
df_fseason = df_fseason.merge(reg_last5seasons_rate_fg, on=["TeamID"], how="left")

reg_last5seasons_rate_fg2 = (
    df5.groupby(["TeamID"])["reg_%fg2"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg2": "reg_last5seasons_rate_fg2"})
)
df_fseason = df_fseason.merge(reg_last5seasons_rate_fg2, on=["TeamID"], how="left")

reg_last5seasons_rate_fg3 = (
    df5.groupby(["TeamID"])["reg_%fg3"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%fg3": "reg_last5seasons_rate_fg3"})
)
df_fseason = df_fseason.merge(reg_last5seasons_rate_fg3, on=["TeamID"], how="left")

reg_last5seasons_rate_ft = (
    df5.groupby(["TeamID"])["reg_%ft"]
    .mean()
    .reset_index()
    .rename(columns={"reg_%ft": "reg_last5seasons_rate_ft"})
)
df_fseason = df_fseason.merge(reg_last5seasons_rate_ft, on=["TeamID"], how="left")

reg_last5seasons_avg_or = (
    df5.groupby(["TeamID"])["reg_avg_or"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_or": "reg_last5seasons_avg_or"})
)
df_fseason = df_fseason.merge(reg_last5seasons_avg_or, on=["TeamID"], how="left")

reg_last5seasons_avg_dr = (
    df5.groupby(["TeamID"])["reg_avg_dr"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_dr": "reg_last5seasons_avg_dr"})
)
df_fseason = df_fseason.merge(reg_last5seasons_avg_dr, on=["TeamID"], how="left")

reg_last5seasons_avg_ast = (
    df5.groupby(["TeamID"])["reg_avg_ast"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_ast": "reg_last5seasons_avg_ast"})
)
df_fseason = df_fseason.merge(reg_last5seasons_avg_ast, on=["TeamID"], how="left")

reg_last5seasons_avg_to = (
    df5.groupby(["TeamID"])["reg_avg_to"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_to": "reg_last5seasons_avg_to"})
)
df_fseason = df_fseason.merge(reg_last5seasons_avg_to, on=["TeamID"], how="left")

reg_last5seasons_avg_stl = (
    df5.groupby(["TeamID"])["reg_avg_stl"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_stl": "reg_last5seasons_avg_stl"})
)
df_fseason = df_fseason.merge(reg_last5seasons_avg_stl, on=["TeamID"], how="left")

reg_last5seasons_avg_blk = (
    df5.groupby(["TeamID"])["reg_avg_blk"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_blk": "reg_last5seasons_avg_blk"})
)
df_fseason = df_fseason.merge(reg_last5seasons_avg_blk, on=["TeamID"], how="left")

reg_last5seasons_avg_pf = (
    df5.groupby(["TeamID"])["reg_avg_pf"]
    .mean()
    .reset_index()
    .rename(columns={"reg_avg_pf": "reg_last5seasons_avg_pf"})
)
df_fseason = df_fseason.merge(reg_last5seasons_avg_pf, on=["TeamID"], how="left")

df_fseason.to_csv("../..//data/interim/m_reg_stat.csv")
print("Your results saved")
