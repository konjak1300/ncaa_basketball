"""
Preprocess tournament data
"""
# pylint: disable=no-member
# pylint: disable=unsubscriptable-object
# pylint: disable=unsupported-assignment-operation

import click
import pandas as pd

# INPUT_PATH = "../../data/raw/WNCAATourneyDetailedResults.csv"
# OUTPUT_PATH = "../../data/interim/w_ncaa.csv"


@click.command()
@click.argument("input_path", type=click.Path())
@click.argument("output_path", type=click.Path())
def preprocess_ncaa_data(input_path: str, output_path: str):
    """
    Function for raw data preprocessing
    :param input_path: Path to read data
    :param output_path: Path to save data
    :return:
    """

    # Чтение файла возможность смены под данные для женских команд
    df = pd.read_csv(input_path)

    # Подсчёт побед и поражений у каждой команды
    ncaa_w_count = df.groupby(["Season", "WTeamID"]).count()
    ncaa_w_count = ncaa_w_count.reset_index()[["Season", "WTeamID", "DayNum"]].rename(
        columns={"DayNum": "ncaa_w_count", "WTeamID": "TeamID"}
    )
    ncaa_l_count = df.groupby(["Season", "LTeamID"]).count()
    ncaa_l_count = ncaa_l_count.reset_index()[["Season", "LTeamID", "DayNum"]].rename(
        columns={"DayNum": "ncaa_l_count", "LTeamID": "TeamID"}
    )
    # Удаляем столбец со значениями типа object
    df.drop(["WLoc"], axis=1, inplace=True)

    # Новый столбец с разницей забитых и пропущенных
    df["ncaa_score_diff"] = df["WScore"] - df["LScore"]

    # Средняя разница забитых и пропущенных в выйгрышных матчах
    ncaa_w_avg_score_diff = (
        df.groupby(["Season", "WTeamID"]).ncaa_score_diff.mean().reset_index()
    )
    ncaa_w_avg_score_diff = ncaa_w_avg_score_diff[
        ["Season", "WTeamID", "ncaa_score_diff"]
    ].rename(columns={"ncaa_score_diff": "ncaa_w_avg_score_diff", "WTeamID": "TeamID"})
    # Средняя разница забитых и пропущенных в пройгрышных матчах
    ncaa_l_avg_score_diff = (
        df.groupby(["Season", "LTeamID"])["ncaa_score_diff"].mean().reset_index()
    )
    ncaa_l_avg_score_diff = ncaa_l_avg_score_diff[
        ["Season", "LTeamID", "ncaa_score_diff"]
    ].rename(columns={"ncaa_score_diff": "ncaa_l_avg_score_diff", "LTeamID": "TeamID"})
    # Подготовка основной таблицы из всех команд, которые выйгрывали и проигрывали
    ncaa_win_stat = (
        df.groupby(["Season", "WTeamID"])
        .count()
        .reset_index()[["Season", "WTeamID"]]
        .rename(columns={"WTeamID": "TeamID"})
    )
    ncaa_lose_stat = (
        df.groupby(["Season", "LTeamID"])
        .count()
        .reset_index()[["Season", "LTeamID"]]
        .rename(columns={"LTeamID": "TeamID"})
    )
    result_stat = (
        pd.concat([ncaa_win_stat, ncaa_lose_stat], axis=0)
        .drop_duplicates()
        .sort_values(["Season", "TeamID"])
        .reset_index(drop=True)
    )
    # Соединяем с таблицей количество побед и поражений,
    # и среднюю разницы забитых и пропущенных в выйгрышных и пройгрышных матчах

    result_stat = result_stat.merge(ncaa_w_count, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_count, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(
        ncaa_w_avg_score_diff, on=["Season", "TeamID"], how="left"
    )
    result_stat = result_stat.merge(
        ncaa_l_avg_score_diff, on=["Season", "TeamID"], how="left"
    )
    result_stat.fillna(0, inplace=True)
    # % побед
    result_stat["ncaa_win_rate"] = result_stat["ncaa_w_count"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Средняя разница забитых и пропущенных
    result_stat["ncaa_avg_diff"] = (
        (result_stat["ncaa_w_count"] * result_stat["ncaa_w_avg_score_diff"])
        - (result_stat["ncaa_l_count"] * result_stat["ncaa_l_avg_score_diff"])
    ) / (result_stat["ncaa_l_count"] + result_stat["ncaa_w_count"])
    # Сколько забито в выйгрышных и пройгрышных матчах
    ncaa_w_self_pts = (
        df.groupby(["Season", "WTeamID"])["WScore"]
        .sum()
        .reset_index()
        .rename(columns={"WScore": "ncaa_w_self_pts", "WTeamID": "TeamID"})
    )
    ncaa_l_self_pts = (
        df.groupby(["Season", "LTeamID"])["LScore"]
        .sum()
        .reset_index()
        .rename(columns={"LScore": "ncaa_l_self_pts", "LTeamID": "TeamID"})
    )
    # Сколько забито противником в выйгрышных и пройгрышных матчах
    ncaa_w_opp_pts = (
        df.groupby(["Season", "WTeamID"])["LScore"]
        .sum()
        .reset_index()
        .rename(columns={"LScore": "ncaa_w_opp_pts", "WTeamID": "TeamID"})
    )
    ncaa_l_opp_pts = (
        df.groupby(["Season", "LTeamID"])["WScore"]
        .sum()
        .reset_index()
        .rename(columns={"WScore": "ncaa_l_opp_pts", "LTeamID": "TeamID"})
    )
    result_stat = result_stat.merge(
        ncaa_w_self_pts, on=["Season", "TeamID"], how="left"
    )
    result_stat = result_stat.merge(
        ncaa_l_self_pts, on=["Season", "TeamID"], how="left"
    )
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_self_pts"] = (
        result_stat["ncaa_l_self_pts"] + result_stat["ncaa_w_self_pts"]
    )
    result_stat = result_stat.merge(ncaa_w_opp_pts, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_opp_pts, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_opp_pts"] = (
        result_stat["ncaa_l_opp_pts"] + result_stat["ncaa_w_opp_pts"]
    )
    result_stat["ncaa_self_pts_per_game"] = (result_stat["ncaa_total_self_pts"]) / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    result_stat["ncaa_opp_pts_per_game"] = (result_stat["ncaa_total_opp_pts"]) / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Забитых бросков с игры
    ncaa_w_fgm = (
        df.groupby(["Season", "WTeamID"])["WFGM"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WFGM": "ncaa_w_fgm"})
    )
    ncaa_l_fgm = (
        df.groupby(["Season", "LTeamID"])["LFGM"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LFGM": "ncaa_l_fgm"})
    )
    result_stat = result_stat.merge(ncaa_w_fgm, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_fgm, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_fgm"] = (
        result_stat["ncaa_l_fgm"] + result_stat["ncaa_w_fgm"]
    )
    result_stat["ncaa_avg_fgm"] = result_stat["ncaa_total_fgm"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Бросков с игры
    ncaa_w_fga = (
        df.groupby(["Season", "WTeamID"])["WFGA"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WFGA": "ncaa_w_fga"})
    )
    ncaa_l_fga = (
        df.groupby(["Season", "LTeamID"])["LFGA"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LFGA": "ncaa_l_fga"})
    )
    result_stat = result_stat.merge(ncaa_w_fga, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_fga, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_fga"] = (
        result_stat["ncaa_w_fga"] + result_stat["ncaa_l_fga"]
    )
    result_stat["ncaa_avg_fga"] = result_stat["ncaa_total_fga"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # % попадания бросков с игры
    result_stat["ncaa_%fg"] = (
        result_stat["ncaa_total_fgm"] / result_stat["ncaa_total_fga"]
    )
    # Забитых 3ех
    ncaa_w_fgm3 = (
        df.groupby(["Season", "WTeamID"])["WFGM3"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WFGM3": "ncaa_w_fgm3"})
    )
    ncaa_l_fgm3 = (
        df.groupby(["Season", "LTeamID"])["LFGM3"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LFGM3": "ncaa_l_fgm3"})
    )
    result_stat = result_stat.merge(ncaa_w_fgm3, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_fgm3, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_fgm3"] = (
        result_stat["ncaa_w_fgm3"] + result_stat["ncaa_l_fgm3"]
    )
    result_stat["ncaa_avg_fgm3"] = result_stat["ncaa_total_fgm3"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Всего 3ех
    ncaa_w_fga3 = (
        df.groupby(["Season", "WTeamID"])["WFGA3"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WFGA3": "ncaa_w_fga3"})
    )
    ncaa_l_fga3 = (
        df.groupby(["Season", "LTeamID"])["LFGA3"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LFGA3": "ncaa_l_fga3"})
    )
    result_stat = result_stat.merge(ncaa_w_fga3, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_fga3, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_fga3"] = (
        result_stat["ncaa_w_fga3"] + result_stat["ncaa_l_fga3"]
    )
    result_stat["ncaa_avg_fga3"] = result_stat["ncaa_total_fga3"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # % попадания 3ех
    result_stat["ncaa_%fg3"] = (
        result_stat["ncaa_total_fgm3"] / result_stat["ncaa_total_fga3"]
    )
    # % попадания средних
    result_stat["ncaa_%fg2"] = (
        result_stat["ncaa_total_fgm"] - result_stat["ncaa_total_fgm3"]
    ) / (result_stat["ncaa_total_fga"] - result_stat["ncaa_total_fga3"])
    # Забитых штрафных
    ncaa_w_ftm = (
        df.groupby(["Season", "WTeamID"])["WFTM"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WFTM": "ncaa_w_ftm"})
    )
    ncaa_l_ftm = (
        df.groupby(["Season", "LTeamID"])["LFTM"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LFTM": "ncaa_l_ftm"})
    )
    result_stat = result_stat.merge(ncaa_w_ftm, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_ftm, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_ftm"] = (
        result_stat["ncaa_w_ftm"] + result_stat["ncaa_l_ftm"]
    )
    result_stat["ncaa_avg_ftm"] = result_stat["ncaa_total_ftm"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Всего штрафных
    ncaa_w_fta = (
        df.groupby(["Season", "WTeamID"])["WFTA"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WFTA": "ncaa_w_fta"})
    )
    ncaa_l_fta = (
        df.groupby(["Season", "LTeamID"])["LFTA"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LFTA": "ncaa_l_fta"})
    )
    result_stat = result_stat.merge(ncaa_w_fta, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_fta, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_fta"] = (
        result_stat["ncaa_w_fta"] + result_stat["ncaa_l_fta"]
    )
    result_stat["ncaa_avg_fta"] = result_stat["ncaa_total_fta"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # % попадания штрафных
    result_stat["ncaa_%ft"] = (
        result_stat["ncaa_total_ftm"] / result_stat["ncaa_total_fta"]
    )
    # Подборы в нападении
    ncaa_w_or = (
        df.groupby(["Season", "WTeamID"])["WOR"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WOR": "ncaa_w_or"})
    )
    ncaa_l_or = (
        df.groupby(["Season", "LTeamID"])["LOR"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LOR": "ncaa_l_or"})
    )
    result_stat = result_stat.merge(ncaa_w_or, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_or, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_or"] = result_stat["ncaa_w_or"] + result_stat["ncaa_l_or"]
    result_stat["ncaa_avg_or"] = result_stat["ncaa_total_or"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Подборы в защите
    ncaa_w_dr = (
        df.groupby(["Season", "WTeamID"])["WDR"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WDR": "ncaa_w_dr"})
    )
    ncaa_l_dr = (
        df.groupby(["Season", "LTeamID"])["LDR"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LDR": "ncaa_l_dr"})
    )
    result_stat = result_stat.merge(ncaa_w_dr, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_dr, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_dr"] = result_stat["ncaa_w_dr"] + result_stat["ncaa_l_dr"]
    result_stat["ncaa_avg_dr"] = result_stat["ncaa_total_dr"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Передачи
    ncaa_w_ast = (
        df.groupby(["Season", "WTeamID"])["WAst"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WAst": "ncaa_w_ast"})
    )
    ncaa_l_ast = (
        df.groupby(["Season", "LTeamID"])["LAst"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LAst": "ncaa_l_ast"})
    )
    result_stat = result_stat.merge(ncaa_w_ast, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_ast, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_ast"] = (
        result_stat["ncaa_w_ast"] + result_stat["ncaa_l_ast"]
    )
    result_stat["ncaa_avg_ast"] = result_stat["ncaa_total_ast"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Потери
    ncaa_w_to = (
        df.groupby(["Season", "WTeamID"])["WTO"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WTO": "ncaa_w_to"})
    )
    ncaa_l_to = (
        df.groupby(["Season", "LTeamID"])["LTO"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LTO": "ncaa_l_to"})
    )
    result_stat = result_stat.merge(ncaa_w_to, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_to, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_to"] = result_stat["ncaa_w_to"] + result_stat["ncaa_l_to"]
    result_stat["ncaa_avg_to"] = result_stat["ncaa_total_to"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Перехваты
    ncaa_w_stl = (
        df.groupby(["Season", "WTeamID"])["WStl"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WStl": "ncaa_w_stl"})
    )
    ncaa_l_stl = (
        df.groupby(["Season", "LTeamID"])["LStl"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LStl": "ncaa_l_stl"})
    )
    result_stat = result_stat.merge(ncaa_w_stl, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_stl, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_stl"] = (
        result_stat["ncaa_w_stl"] + result_stat["ncaa_l_stl"]
    )
    result_stat["ncaa_avg_stl"] = result_stat["ncaa_total_stl"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Блоки
    ncaa_w_blk = (
        df.groupby(["Season", "WTeamID"])["WBlk"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WBlk": "ncaa_w_blk"})
    )
    ncaa_l_blk = (
        df.groupby(["Season", "LTeamID"])["LBlk"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LBlk": "ncaa_l_blk"})
    )
    result_stat = result_stat.merge(ncaa_w_blk, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_blk, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_blk"] = (
        result_stat["ncaa_w_blk"] + result_stat["ncaa_l_blk"]
    )
    result_stat["ncaa_avg_blk"] = result_stat["ncaa_total_blk"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Нарушения
    ncaa_w_pf = (
        df.groupby(["Season", "WTeamID"])["WPF"]
        .sum()
        .reset_index()
        .rename(columns={"WTeamID": "TeamID", "WPF": "ncaa_w_pf"})
    )
    ncaa_l_pf = (
        df.groupby(["Season", "LTeamID"])["LPF"]
        .sum()
        .reset_index()
        .rename(columns={"LTeamID": "TeamID", "LPF": "ncaa_l_pf"})
    )
    result_stat = result_stat.merge(ncaa_w_pf, on=["Season", "TeamID"], how="left")
    result_stat = result_stat.merge(ncaa_l_pf, on=["Season", "TeamID"], how="left")
    result_stat.fillna(0, inplace=True)
    result_stat["ncaa_total_pf"] = result_stat["ncaa_w_pf"] + result_stat["ncaa_l_pf"]
    result_stat["ncaa_avg_pf"] = result_stat["ncaa_total_pf"] / (
        result_stat["ncaa_w_count"] + result_stat["ncaa_l_count"]
    )
    # Сохранение файла, меняем название если используем данные для женских команд
    result_stat.to_csv(output_path)
    print("Your results saved")

    return None


preprocess_ncaa_data()
