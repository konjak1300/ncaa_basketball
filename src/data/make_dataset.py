"""
Check that raw data format is correct and preprocess it
"""

import pandas as pd


df_cities = pd.read_parquet("data/raw/Cities.csv")

print(df_cities.columns)
print(df_cities.head())
print(df_cities.shape)
print(df_cities.head(6))

df_cities.to_csv("data/interim/cities_preprocessed.csv")
