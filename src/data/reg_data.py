"""
Preprocess regular season data
"""
# pylint: disable=no-member
# pylint: disable=unsubscriptable-object
# pylint: disable=unsupported-assignment-operation

import pandas as pd

# Чтение файла возможность смены под данные для женских команд
df = pd.read_csv("../../data/raw/MRegularSeasonDetailedResults.csv")

# Подсчёт побед и поражений у каждой команды
reg_w_count = df.groupby(["Season", "WTeamID"]).count()
reg_w_count = reg_w_count.reset_index()[["Season", "WTeamID", "DayNum"]].rename(
    columns={"DayNum": "reg_w_count", "WTeamID": "TeamID"}
)
reg_l_count = df.groupby(["Season", "LTeamID"]).count()
reg_l_count = reg_l_count.reset_index()[["Season", "LTeamID", "DayNum"]].rename(
    columns={"DayNum": "reg_l_count", "LTeamID": "TeamID"}
)
# Удаляем столбец со значениями типа object
df.drop(["WLoc"], axis=1, inplace=True)
# Новый столбец с разницей забитых и пропущенных
df["reg_score_diff"] = df["WScore"] - df["LScore"]
# Средняя разница забитых и пропущенных в выйгрышных матчах
reg_w_avg_score_diff = (
    df.groupby(["Season", "WTeamID"])["reg_score_diff"].mean().reset_index()
)
reg_w_avg_score_diff = reg_w_avg_score_diff[
    ["Season", "WTeamID", "reg_score_diff"]
].rename(columns={"reg_score_diff": "reg_w_avg_score_diff", "WTeamID": "TeamID"})
# Средняя разница забитых и пропущенных в пройгрышных матчах
reg_l_avg_score_diff = (
    df.groupby(["Season", "LTeamID"])["reg_score_diff"].mean().reset_index()
)
reg_l_avg_score_diff = reg_l_avg_score_diff[
    ["Season", "LTeamID", "reg_score_diff"]
].rename(columns={"reg_score_diff": "reg_l_avg_score_diff", "LTeamID": "TeamID"})
# Подготовка основной таблицы из всех команд, которые выйгрывали и проигрывали
reg_win_stat = (
    df.groupby(["Season", "WTeamID"])
    .count()
    .reset_index()[["Season", "WTeamID"]]
    .rename(columns={"WTeamID": "TeamID"})
)
reg_lose_stat = (
    df.groupby(["Season", "LTeamID"])
    .count()
    .reset_index()[["Season", "LTeamID"]]
    .rename(columns={"LTeamID": "TeamID"})
)
result_stat = (
    pd.concat([reg_win_stat, reg_lose_stat], axis=0)
    .drop_duplicates()
    .sort_values(["Season", "TeamID"])
    .reset_index(drop=True)
)
# Соединяем с таблицей количество побед и поражений,
# и среднюю разницы забитых и пропущенных в выйгрышных и пройгрышных матчах

result_stat = result_stat.merge(reg_w_count, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_count, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(
    reg_w_avg_score_diff, on=["Season", "TeamID"], how="left"
)
result_stat = result_stat.merge(
    reg_l_avg_score_diff, on=["Season", "TeamID"], how="left"
)
result_stat.fillna(0, inplace=True)
# % побед
result_stat["reg_win_rate"] = result_stat["reg_w_count"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Средняя разница забитых и пропущенных
result_stat["reg_avg_diff"] = (
    (result_stat["reg_w_count"] * result_stat["reg_w_avg_score_diff"])
    - (result_stat["reg_l_count"] * result_stat["reg_l_avg_score_diff"])
) / (result_stat["reg_l_count"] + result_stat["reg_w_count"])
# Сколько забито в выйгрышных и пройгрышных матчах
reg_w_self_pts = (
    df.groupby(["Season", "WTeamID"])["WScore"]
    .sum()
    .reset_index()
    .rename(columns={"WScore": "reg_w_self_pts", "WTeamID": "TeamID"})
)
reg_l_self_pts = (
    df.groupby(["Season", "LTeamID"])["LScore"]
    .sum()
    .reset_index()
    .rename(columns={"LScore": "reg_l_self_pts", "LTeamID": "TeamID"})
)
# Сколько забито противником в выйгрышных и пройгрышных матчах
reg_w_opp_pts = (
    df.groupby(["Season", "WTeamID"])["LScore"]
    .sum()
    .reset_index()
    .rename(columns={"LScore": "reg_w_opp_pts", "WTeamID": "TeamID"})
)
reg_l_opp_pts = (
    df.groupby(["Season", "LTeamID"])["WScore"]
    .sum()
    .reset_index()
    .rename(columns={"WScore": "reg_l_opp_pts", "LTeamID": "TeamID"})
)
result_stat = result_stat.merge(reg_w_self_pts, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_self_pts, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_self_pts"] = (
    result_stat["reg_l_self_pts"] + result_stat["reg_w_self_pts"]
)
result_stat = result_stat.merge(reg_w_opp_pts, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_opp_pts, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_opp_pts"] = (
    result_stat["reg_l_opp_pts"] + result_stat["reg_w_opp_pts"]
)
result_stat["reg_self_pts_per_game"] = (result_stat["reg_total_self_pts"]) / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
result_stat["reg_opp_pts_per_game"] = (result_stat["reg_total_opp_pts"]) / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Забитых бросков с игры
reg_w_fgm = (
    df.groupby(["Season", "WTeamID"])["WFGM"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WFGM": "reg_w_fgm"})
)
reg_l_fgm = (
    df.groupby(["Season", "LTeamID"])["LFGM"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LFGM": "reg_l_fgm"})
)
result_stat = result_stat.merge(reg_w_fgm, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_fgm, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_fgm"] = result_stat["reg_l_fgm"] + result_stat["reg_w_fgm"]
result_stat["reg_avg_fgm"] = result_stat["reg_total_fgm"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Бросков с игры
reg_w_fga = (
    df.groupby(["Season", "WTeamID"])["WFGA"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WFGA": "reg_w_fga"})
)
reg_l_fga = (
    df.groupby(["Season", "LTeamID"])["LFGA"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LFGA": "reg_l_fga"})
)
result_stat = result_stat.merge(reg_w_fga, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_fga, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_fga"] = result_stat["reg_w_fga"] + result_stat["reg_l_fga"]
result_stat["reg_avg_fga"] = result_stat["reg_total_fga"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# % попадания бросков с игры
result_stat["reg_%fg"] = result_stat["reg_total_fgm"] / result_stat["reg_total_fga"]
# Забитых 3ех
reg_w_fgm3 = (
    df.groupby(["Season", "WTeamID"])["WFGM3"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WFGM3": "reg_w_fgm3"})
)
reg_l_fgm3 = (
    df.groupby(["Season", "LTeamID"])["LFGM3"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LFGM3": "reg_l_fgm3"})
)
result_stat = result_stat.merge(reg_w_fgm3, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_fgm3, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_fgm3"] = result_stat["reg_w_fgm3"] + result_stat["reg_l_fgm3"]
result_stat["reg_avg_fgm3"] = result_stat["reg_total_fgm3"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Всего 3ех
reg_w_fga3 = (
    df.groupby(["Season", "WTeamID"])["WFGA3"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WFGA3": "reg_w_fga3"})
)
reg_l_fga3 = (
    df.groupby(["Season", "LTeamID"])["LFGA3"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LFGA3": "reg_l_fga3"})
)
result_stat = result_stat.merge(reg_w_fga3, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_fga3, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_fga3"] = result_stat["reg_w_fga3"] + result_stat["reg_l_fga3"]
result_stat["reg_avg_fga3"] = result_stat["reg_total_fga3"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# % попадания 3ех
result_stat["reg_%fg3"] = result_stat["reg_total_fgm3"] / result_stat["reg_total_fga3"]
# % попадания средних
result_stat["reg_%fg2"] = (
    result_stat["reg_total_fgm"] - result_stat["reg_total_fgm3"]
) / (result_stat["reg_total_fga"] - result_stat["reg_total_fga3"])
# Забитых штрафных
reg_w_ftm = (
    df.groupby(["Season", "WTeamID"])["WFTM"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WFTM": "reg_w_ftm"})
)
reg_l_ftm = (
    df.groupby(["Season", "LTeamID"])["LFTM"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LFTM": "reg_l_ftm"})
)
result_stat = result_stat.merge(reg_w_ftm, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_ftm, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_ftm"] = result_stat["reg_w_ftm"] + result_stat["reg_l_ftm"]
result_stat["reg_avg_ftm"] = result_stat["reg_total_ftm"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Всего штрафных
reg_w_fta = (
    df.groupby(["Season", "WTeamID"])["WFTA"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WFTA": "reg_w_fta"})
)
reg_l_fta = (
    df.groupby(["Season", "LTeamID"])["LFTA"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LFTA": "reg_l_fta"})
)
result_stat = result_stat.merge(reg_w_fta, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_fta, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_fta"] = result_stat["reg_w_fta"] + result_stat["reg_l_fta"]
result_stat["reg_avg_fta"] = result_stat["reg_total_fta"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# % попадания штрафных
result_stat["reg_%ft"] = result_stat["reg_total_ftm"] / result_stat["reg_total_fta"]
# Подборы в нападении
reg_w_or = (
    df.groupby(["Season", "WTeamID"])["WOR"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WOR": "reg_w_or"})
)
reg_l_or = (
    df.groupby(["Season", "LTeamID"])["LOR"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LOR": "reg_l_or"})
)
result_stat = result_stat.merge(reg_w_or, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_or, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_or"] = result_stat["reg_w_or"] + result_stat["reg_l_or"]
result_stat["reg_avg_or"] = result_stat["reg_total_or"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Подборы в защите
reg_w_dr = (
    df.groupby(["Season", "WTeamID"])["WDR"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WDR": "reg_w_dr"})
)
reg_l_dr = (
    df.groupby(["Season", "LTeamID"])["LDR"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LDR": "reg_l_dr"})
)
result_stat = result_stat.merge(reg_w_dr, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_dr, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_dr"] = result_stat["reg_w_dr"] + result_stat["reg_l_dr"]
result_stat["reg_avg_dr"] = result_stat["reg_total_dr"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Передачи
reg_w_ast = (
    df.groupby(["Season", "WTeamID"])["WAst"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WAst": "reg_w_ast"})
)
reg_l_ast = (
    df.groupby(["Season", "LTeamID"])["LAst"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LAst": "reg_l_ast"})
)
result_stat = result_stat.merge(reg_w_ast, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_ast, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_ast"] = result_stat["reg_w_ast"] + result_stat["reg_l_ast"]
result_stat["reg_avg_ast"] = result_stat["reg_total_ast"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Потери
reg_w_to = (
    df.groupby(["Season", "WTeamID"])["WTO"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WTO": "reg_w_to"})
)
reg_l_to = (
    df.groupby(["Season", "LTeamID"])["LTO"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LTO": "reg_l_to"})
)
result_stat = result_stat.merge(reg_w_to, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_to, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_to"] = result_stat["reg_w_to"] + result_stat["reg_l_to"]
result_stat["reg_avg_to"] = result_stat["reg_total_to"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Перехваты
reg_w_stl = (
    df.groupby(["Season", "WTeamID"])["WStl"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WStl": "reg_w_stl"})
)
reg_l_stl = (
    df.groupby(["Season", "LTeamID"])["LStl"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LStl": "reg_l_stl"})
)
result_stat = result_stat.merge(reg_w_stl, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_stl, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_stl"] = result_stat["reg_w_stl"] + result_stat["reg_l_stl"]
result_stat["reg_avg_stl"] = result_stat["reg_total_stl"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Блоки
reg_w_blk = (
    df.groupby(["Season", "WTeamID"])["WBlk"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WBlk": "reg_w_blk"})
)
reg_l_blk = (
    df.groupby(["Season", "LTeamID"])["LBlk"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LBlk": "reg_l_blk"})
)
result_stat = result_stat.merge(reg_w_blk, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_blk, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_blk"] = result_stat["reg_w_blk"] + result_stat["reg_l_blk"]
result_stat["reg_avg_blk"] = result_stat["reg_total_blk"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Нарушения
reg_w_pf = (
    df.groupby(["Season", "WTeamID"])["WPF"]
    .sum()
    .reset_index()
    .rename(columns={"WTeamID": "TeamID", "WPF": "reg_w_pf"})
)
reg_l_pf = (
    df.groupby(["Season", "LTeamID"])["LPF"]
    .sum()
    .reset_index()
    .rename(columns={"LTeamID": "TeamID", "LPF": "reg_l_pf"})
)
result_stat = result_stat.merge(reg_w_pf, on=["Season", "TeamID"], how="left")
result_stat = result_stat.merge(reg_l_pf, on=["Season", "TeamID"], how="left")
result_stat.fillna(0, inplace=True)
result_stat["reg_total_pf"] = result_stat["reg_w_pf"] + result_stat["reg_l_pf"]
result_stat["reg_avg_pf"] = result_stat["reg_total_pf"] / (
    result_stat["reg_w_count"] + result_stat["reg_l_count"]
)
# Сохранение файла, меняем название если используем данные для женских команд
result_stat.to_csv("../..//data/interim/m_reg.csv")
print("Your results saved")
